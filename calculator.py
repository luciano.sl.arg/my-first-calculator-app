#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Basic calculator

import sys
from tkinter import Tk,Frame,Canvas,Button

def isdigit(num):
    assert num != ''
    return (num[0] =='-' and num[1:].isdigit()) or num.isdigit()

class Display(Canvas):

    def __init__(self,master):
        super().__init__(master=master,height=100,highlightthickness=0,bg='white',width=300)
        self.bind('<Expose>',self.do_draw)
        self.text='0'
        self.pack(expand=1,fill='both')

    def do_draw(self,event):
        self.delete('all')
        self.create_text(
                self['width'],
                self['height'],
                text=self.text,
                font=('sans',18,'bold'),
                anchor='se',
               )

    def clear(self,event=None):
        self.write('0')

    def write(self,text):
        self.text=text
        self.do_draw(None)

class Keypad(Frame):
    
    buttons=(
            ('(',')','\u2794','C','\u23fb'),
            ('7','8','9','+','*'),
            ('4','5','6','-','/'),
            ('1','2','3'),
            ('\u2213','0','.'),
            )
    def __init__(self,master):
        super().__init__(master)
        for i,row in enumerate(self.buttons):
            for j,label in enumerate(row):
                btn=Button(self,text=label,font=('monospace',18,'bold'))
                btn.grid(row=i,column=j,sticky='nsew',ipadx=10,ipady=10 )
                btn.bind('<Button-1>',func=self.on_click)

            btn=Button(self,text='=',font=('monospace',18,'bold') )
            btn.grid(row=3,column=3,columnspan=2,rowspan=2,sticky='nsew',ipadx=10,ipady=10 )
            btn.bind('<Button-1>',func=self.on_click)


    def on_click(self,event):

        key=event.widget.cget('text')
        # normalize keys
        if key == '\u2794':
            _key="back"
        elif key == '\u23fb':
            _key="off"
        elif key == '\u2213':
            _key='plus-minus'
        else:
            _key=key

        self.master.key_pressed(_key)



class Calculator(Frame):

    def __init__(self,master):
        super().__init__(master,borderwidth=0)
        self.display=Display(self)
        self.display.pack(fill='both')
        self.keypad=Keypad(self)
        self.keypad.pack(fill='both',expand=1)
        self.text=''
        self.clear_display=False
        self.last_key=''

    # handle input
    def key_pressed(self,key:str):
        if key == 'off':
            sys.exit(0)
        elif key == '=':
            try:
                self.text=str(eval(self.text))
            except:
                self.text='Error Expr.'

        elif key == 'C':
            self.text='0'
        elif key == 'back':
            self.text=self.text[0:-1] or '0'
        elif key == 'plus-minus' :
            self.text= '-'+self.text if '-' != self.text[0] else self.text[1:]
        else:
            self.text+=key
            self.text=self.text.lstrip('0')
        
        self.display.write(self.text)
 
def main():
    tk=Tk()
    tk.wm_title("Calculator")
    tk.resizable(False,False)
    app=Calculator(tk)
    app.pack(fill='both')
    tk.mainloop()

if __name__ == '__main__':
    main()
